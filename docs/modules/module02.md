# 2. Conception assistée par ordinateur


L'excercie est de modéliser le bureau Boomerang ou un plug-in à celui-ci.
Les formes du bureau étant assez complexe, je crois que c'est un bon excercie pour me familiariser avec Fusion 360 que d'essayer de reproduire les formes organique du bureau Boomerang.
L'objectif ici n'est pas de le reproduire à l'identique mais bien de comprendre et d'apprehender les outils me permettant d'obtenir un résultat similaire.

1. **modélisation SCUPT**

Dans Fusion 360, choisir l'outil forme & create a box. Definir les dimensions de la bite shouaiter. Les flèches sur les axes x,Y,Z permettent de d"finir le nombre de surface par face.

- un nombre bas de surface par face entrainera une boite courbé avec des arrêtes peu saillantes 
- un nombre élevé de surface par produira une boite a tendance droite avec des arrêtes plus saillants


![](../images/model1.jpg)

Un click droit permet d'ouvrir le panneau de commande et de choisir **modifier la surface**. En tirant poussant le curseur, la boite se déforme. Il est également possible d'affiner les déformations en selectionnant les arrêtes et les points composant la forme.

![](../images/model2.jpg)
![](../images/model3.jpg)


Le bureau a d'abord été pensé comme un petit volume qui s'étend ( et non comme un gros volume duquel on réduit de la matière), il est dès lors plus facile d'en controler les paramètres.
Les faces ont été renfoncer pour permettre l'assise.

![](../images/model4.jpg)


Lors de la fermeture de la forme ( finish form), un message d'erreur apparait: la forme ne peut êêtre fermé pour cause de problème d'interraction de faces. Il faut alors reprendre le contôle pour modifier le volume et ainsi fournir une boite où les faces de celle-ci ne s'intersectent pas.

![](../images/model5.jpg)


Afin de réaliser la surface plane du bureau, j'utilise l'outil **COMBINE**. Au préalable, créer une forme coupante, ici un parallepidèpe rectangle parallèle au plan horizontal. En selectionnat l'outil combine, il est possible d'extraire une forme d'une autre.

Pour se faire selectionner la forme à soustraire et ensuite la forme qui soustrait, choisir l'option **CUT** dans operation

![](../images/model6.jpg)


Répeter l'opération en fonction du résultat voulu. Il est plus facile de s'aligner sur les vues orthogonales proposé par Fusion 360, ont peut ainsi disposer la forme a soustraire de manière précise sans être faussé par la perspective.
![](../images/model7.jpg)
![](../images/model8.jpg)
![](../images/model9.jpg)

**résultat final**

Le résultat obtenu est à mon gout satisfaisant mais perfectible. Il est assez difficile de maitriser l'outil form et sculpt de Fusion 360. Ne disposant pas des documents techniques du bureau Boomerang, l'objectif était ici de comprendre le logiciel Fusion 360 et d'avoir un apperçu de ses cpacités.

![](../images/model10.jpg)


2. **engrenage** SPURGEAR

Pour créer des engrenages, pignon et crémaillere , ouvir  SPURGEAR dans _Outil/Compléments/Scripts et Compléments_.

Module: plus il à la possibilité de transmetter de force et plus le pignon sera gros
Dent: en fonction de la démultiplication voulu
Pitch Diameter : calcul automatiquement la distance où vient s'aplliquer les efforts
