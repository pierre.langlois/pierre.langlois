# 3. Impression 3D


## Import

Après avoir finalisé la forme sur Fusion 360, il est temps de la passer dans **Slicer Prusa**, un logiciel permetant de transformer le dossier **.slt** fourni par Fusion et de le transformer en **.gcode* ; un language que l'imprimante Prusa va interprété pour imprimer la pièce.

## Réglages

L'objet a été tourné et posé sur " la face écriture" de façon à avoir une surface lisse sur celle-ci ( car en contact avec le plateau de l'imprimante. De plus, la pièce s'imprimera mieux car la face en contact avec le sol n'est pas très grande et l'angle partant de celle ci est assez important.

![](../images/slpr5.jpg)

Les supports sont installés partout avec un remplissage gyroïde de 5%. 

![](../images/slpr1.jpg)
![](../images/boomeranginside.jpg)

Lors de l'impression le patern spacing à été laissé en paramètre d'usine a 2mm. **Cette écart doit être modifié car il a posé problème lors de retrait des supports internes.**

![](../images/boomerang1.jpg)

![](../images/slpr2.jpg)
![](../images/slpr3.jpg)

![](../images/boomerang2.jpg)

Un écartement de 6mm aurait été, je pense, suffisant. La pièce n'a pas été ré-imprimé avec ce réglage afin d'économiser la matière et de laisser du temps de travail disponible aux autres.

![](../images/slpr4.jpg)

Pour plus d'informations concernant les réglages de Prusa Slicer et du gcode, voici un [lien](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/clementine.benyakhou/-/blob/master/docs/modules/module03.md) vers de Git de Clémentine qui documente très bien cela 
## Photos

![](../images/boomerang3.jpg)
![](../images/boomerang4.jpg)
![](../images/boomerang5.jpg)
![](../images/boomerang6.jpg)
